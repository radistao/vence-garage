# The Garage

This application simulates multilevel garage system behavior.  

#### Table of Contents
1. [Task Description](#markdown-header-task-description)
2. [Application Building and Executing](#markdown-header-application-building-and-executing)
3. [Implementation Notes and Issues](#markdown-header-implementation-notes-and-issues)
    * [Potential issues](#markdown-header-potential-issues)

4. [Documentation](#markdown-header-documentation)

## Task Description

Main interaction interface is `com.radistao.garage.IGarage`, which has required methods for the description:

> Your task is to develop a simulation program for the garage.
> Vehicles should be able to enter and exit the garage – the garage should then assign
>a free space or reject the vehicle if there are no more free parking lots.
>
>The manager of the garage should be able to ask the system for the location of a specific
> vehicle. The response should include the parking level and the assigned parking lot.
> Also, the number of free parking lots should be queryable.

So, `com.radistao.garage.IGarage` has the next methods:

* `int getFreeLotsCount()`
* `ParkingPlace tryPlaceVehicle(IVehicle vehicle)`
* `ParkingPlace findVehicle(IVehicle vehicle)`
* `IVehicle releasePlace(ParkingPlace place)` and ` IVehicle releasePlace(int level, int lot)`

To instantiate the interface you could use `com.radistao.garage.factory.GarageFactory` 
with `com.radistao.garage.factory.LevelFactory` for levels building.


## Application Building and Executing
Maven project object model *pom.xml* is available in project root directory.
By default the application requires `JDK 1.8` and depends only on `JUnit 4.11`.
Run `mvn package` (`mvn install` if install required) to compile, test and package application. 
Generated application *jar* application will be available in *target* directory (relative *.m2* directory for `install` maven lifecycle).
To test the application after build from project root run from project something like: 

```sh
$ java -cp target/vence-garage-1.1.jar com.radistao.GarageApp
```

## Implementation Notes and Issues
The task description was too short and unspecified in a lot of moments. 
As we didn't have a chance to discuss before implementation, the simplest solution was realized. 
This means it works robust and fast on *expected real* data: tens of levels with hundreds of parking lots, which makes about 10^5 — 10^6 lots. 
On such data application works instant on modern system.

Application used linear memory proportional to total lots number (`levels * lot per level`).

### Potential issues
* Some of implementation methods (`tryPlaceVehicle` and `findVehicle`) are implemented using linear search. 
If we use modern not high-loaded system, and lots capacity is about hundred thousands (see section above) this is fine. 
But this may cause problems on slow systems (for example, implemented on micro-controllers, or high-loaded system, or systems with limited processor time). 
In this case other solution should be discussed and implemented, with certain garage capacity bounds, execution time, memory limits and methods order of growth. 
Base on memory and time requirements may be applied solution on *Hash Maps* or *Binary Heaps*, which will guarantee constant or logarithmic access times, but implementation complexity (and as a result - time) in this case is higher.

* In the task there was no any description about potential cars duplication (identifier, plate), when *the manager* tries to put a vehicle to the parking which already exist. 
For now the system puts such a vehicle to garage without any exceptions, so duplicate cars would be placed in the garage. When than search is performed first found vehicle with such identifier is returned. 
If the customer would like to avoid such a case with some alerting (report to police about frauds ;) ) we should decide and probably re-factor search method to work instantly or at least logarithmic, because this operation will be executed before every car placing.

## Documentation
*JavaDoc* documentation is generated and available in `/docs` directory