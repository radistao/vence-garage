package com.radistao.garage;


import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ParkingPlaceTest {

  private ParkingPlace pl;

  @Before
  public void setUp() throws Exception {
    pl = new ParkingPlace(4, 5);
  }

  @Test
  public void testConstructor() throws Exception {
    assertEquals(4, pl.getLevel());
    assertEquals(5, pl.getLot());
  }

  @Test
  public void testEquals() throws Exception {
    assertEquals(pl, pl);
    assertEquals(pl, new ParkingPlace(4, 5));
    assertEquals(new ParkingPlace(7, 8), new ParkingPlace(7, 8));

    assertNotEquals(pl, new ParkingPlace(5, 4));
    assertNotEquals(pl, new ParkingPlace(5, 5));
    assertNotEquals(pl, new ParkingPlace(4, 4));
    assertNotEquals(new ParkingPlace(8, 8), new ParkingPlace(8, 7));
    assertNotEquals(new ParkingPlace(7, 8), new ParkingPlace(8, 7));
    assertNotEquals(new ParkingPlace(7, 7), new ParkingPlace(8, 7));
  }

  @Test
  public void testHashCode() throws Exception {
    assertEquals(pl.hashCode(), new ParkingPlace(4, 5).hashCode());
    assertEquals(new ParkingPlace(456, 678).hashCode(), new ParkingPlace(456, 678).hashCode());

    // this is not strictly required, that non-equal items have different codes,
    // but in common case we should try to keep hashCode different for items close to each other
    assertNotEquals(pl.hashCode(), new ParkingPlace(5, 4).hashCode());
    assertNotEquals(pl.hashCode(), new ParkingPlace(4, 4).hashCode());
    assertNotEquals(pl.hashCode(), new ParkingPlace(5, 5).hashCode());
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructorException() throws Exception {
    new ParkingPlace(-1, 5);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testConstructorException2() throws Exception {
    new ParkingPlace(7, -6);
  }
}
