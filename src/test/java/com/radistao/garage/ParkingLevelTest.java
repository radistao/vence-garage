package com.radistao.garage;

import com.radistao.exception.GarageException;
import com.radistao.vehicle.Car;
import com.radistao.vehicle.Motorbike;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ParkingLevelTest {

  private ParkingLevel pl;

  @Before
  public void setUp() throws Exception {
    pl = new ParkingLevel(7);
  }

  @After
  public void tearDown() throws Exception {
    pl = null;
  }

  @Test
  public void testConstructor() throws Exception {
    assertEquals(7, pl.getLotsCount());
    assertEquals(7, pl.getFreeLotsCount());

    assertEquals(-1, pl.findVehicle(new Car("test")));
    assertEquals(-1, pl.findVehicle(new Motorbike("test2")));

    for (int i = 0; i < pl.getLotsCount(); i++) {
      assertNull(pl.getVehicle(i));
      assertNull(pl.releasePlace(i));
    }

    assertTrue(pl.findFreeLot() >= 0);
  }

  @Test
  public void testPlaceVehicle() throws Exception {
    Car c = new Car("tyu");
    Motorbike mb = new Motorbike("678");
    Motorbike mb2 = new Motorbike("ghj");

    pl.placeVehicle(0, c);
    assertEquals(7, pl.getLotsCount());
    assertEquals(6, pl.getFreeLotsCount());
    assertTrue(pl.findFreeLot() >= 0);

    int lot = pl.findVehicle(c);
    assertTrue(lot >= 0);
    assertEquals(c, pl.getVehicle(lot));

    pl.placeVehicle(6, mb);
    assertEquals(7, pl.getLotsCount());
    assertEquals(5, pl.getFreeLotsCount());

    lot = pl.findVehicle(mb);
    assertTrue(lot >= 0);
    assertEquals(mb, pl.getVehicle(lot));

    pl.placeVehicle(3, mb2);
    assertEquals(7, pl.getLotsCount());
    assertEquals(4, pl.getFreeLotsCount());

    lot = pl.findVehicle(mb2);
    assertTrue(lot >= 0);
    assertEquals(mb2, pl.getVehicle(lot));
  }

  @Test
  public void testReleasePlace() throws Exception {
    Car c = new Car("tyu");
    Motorbike mb = new Motorbike("678");
    Motorbike mb2 = new Motorbike("ghj");

    pl.placeVehicle(1, c);
    pl.placeVehicle(4, mb);
    pl.placeVehicle(2, mb2);
    assertEquals(4, pl.getFreeLotsCount());
    assertTrue(pl.findVehicle(c) >= 0);

    assertEquals(c, pl.releasePlace(1));
    assertEquals(5, pl.getFreeLotsCount());
    assertEquals(-1, pl.findVehicle(c));

    assertTrue(pl.findVehicle(mb2) >= 0);
    assertEquals(mb2, pl.releasePlace(2));
    assertEquals(6, pl.getFreeLotsCount());
    assertEquals(-1, pl.findVehicle(mb2));

    Car c2 = new Car("qwerty");
    Car c3 = new Car("poiuyt");
    pl.placeVehicle(2, c2);
    assertEquals(5, pl.getFreeLotsCount());
    pl.placeVehicle(3, c3);
    assertEquals(4, pl.getFreeLotsCount());

    assertTrue(pl.findVehicle(c2) >= 0);
    assertTrue(pl.findVehicle(c3) >= 0);
    assertTrue(pl.findVehicle(mb) >= 0);
    assertEquals(-1, pl.findVehicle(c));
    assertEquals(-1, pl.findVehicle(mb2));

    assertEquals(mb, pl.releasePlace(4));
    assertEquals(5, pl.getFreeLotsCount());
    assertEquals(-1, pl.findVehicle(mb));
    assertTrue(pl.findVehicle(c2) >= 0);
    assertTrue(pl.findVehicle(c3) >= 0);

    assertEquals(c2, pl.releasePlace(2));
    assertEquals(6, pl.getFreeLotsCount());
    assertEquals(-1, pl.findVehicle(c2));
    assertTrue(pl.findVehicle(c3) >= 0);

    assertEquals(c3, pl.releasePlace(3));
    assertEquals(7, pl.getFreeLotsCount());

    assertEquals(-1, pl.findVehicle(c));
    assertEquals(-1, pl.findVehicle(c2));
    assertEquals(-1, pl.findVehicle(c3));
    assertEquals(-1, pl.findVehicle(mb));
    assertEquals(-1, pl.findVehicle(mb2));
  }

  @Test
  public void testFindVehicle() throws Exception {
    assertEquals(-1, pl.findVehicle(null));
    assertEquals(-1, pl.findVehicle(new Car("")));
    assertEquals(-1, pl.findVehicle(new Motorbike("rand")));

    Motorbike mb = new Motorbike("mb");
    Motorbike mbClone = new Motorbike("mb");

    pl.placeVehicle(5, mb);
    assertEquals(5, pl.findVehicle(mb));
    assertEquals(pl.findVehicle(mb), pl.findVehicle(mbClone));

    //corner cases
    Assume.assumeTrue(7 == pl.getLotsCount());
    Car car1 = new Car("car1");
    Car car2 = new Car("car2");

    pl.placeVehicle(0, car1);
    pl.placeVehicle(6, car2);

    assertEquals(0, pl.findVehicle(car1));
    assertEquals(6, pl.findVehicle(car2));
    assertEquals(car1, pl.getVehicle(0));
    assertEquals(car2, pl.getVehicle(6));
  }

  @Test
  public void testFindFreeLot() throws Exception {
    for (int i = 0; i < pl.getLotsCount(); i++) {
      int lot = pl.findFreeLot();
      assertTrue(lot >= 0);
      pl.placeVehicle(lot, new Car("car" + Integer.toString(i)));
      assertEquals(pl.getLotsCount() - i - 1, pl.getFreeLotsCount());
    }

    //all slots are filled
    assertEquals(-1, pl.findFreeLot());
    assertEquals(0, pl.getFreeLotsCount());
  }

  //Test exception corner-out cases

  @Test(expected = GarageException.class)
  public void testPlaceOnOccupiedLot() throws Exception {
    pl.placeVehicle(5, new Car("tyu"));
    pl.placeVehicle(5, new Motorbike("sadfjas"));
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testBadIndex() throws Exception {
    pl.placeVehicle(-1, new Car("tyu"));
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void testBadIndex2() throws Exception {
    pl.placeVehicle(7, new Car("tyu"));
  }
}
