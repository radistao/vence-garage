package com.radistao.garage.factory;

import com.radistao.garage.IGarage;
import com.radistao.garage.ParkingLevel;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertNotNull;

public class GarageFactoryTest {
  private LevelFactory lf;
  private GarageFactory gf;
  private IGarage g;

  @Test
  public void testFactory() throws Exception {
    lf = new LevelFactory(12);
    gf = new GarageFactory(lf);
    g = gf.makeGarage(7);
    assertNotNull(g);
    assertEquals(7 * 12, g.getFreeLotsCount());

    gf.setLevelFactory(new ILevelFactory() {
      // progressive level factory
      int perLevel = 1;

      public ParkingLevel makeLevel() {
        return new ParkingLevel(perLevel++);
      }
    });

    g = gf.makeGarage(27);
    assertNotNull(g);
    assertEquals(27 * (27 + 1) / 2, g.getFreeLotsCount());

  }
}
