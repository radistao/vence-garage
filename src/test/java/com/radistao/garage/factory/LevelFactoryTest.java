package com.radistao.garage.factory;

import com.radistao.garage.ParkingLevel;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.assertNotNull;

public class LevelFactoryTest {

  private LevelFactory lf;
  private ParkingLevel pl;


  @Test
  public void testFactory() throws Exception {
    lf = new LevelFactory(5);
    pl = lf.makeLevel();
    assertNotNull(pl);
    assertEquals(5, pl.getLotsCount());
    assertEquals(5, pl.getFreeLotsCount());

    lf = new LevelFactory(42);
    pl = lf.makeLevel();
    assertNotNull(pl);
    assertEquals(42, pl.getLotsCount());
    assertEquals(42, pl.getFreeLotsCount());

  }
}
