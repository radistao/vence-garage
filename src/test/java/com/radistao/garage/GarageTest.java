package com.radistao.garage;

import com.radistao.vehicle.Car;
import com.radistao.vehicle.IVehicle;
import com.radistao.vehicle.Motorbike;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class GarageTest {

  private IGarage g;
  private final int LEVELS = 4;
  private final int LOTS = 5;

  @Before
  public void setUp() throws Exception {
    Garage temp = new Garage();

    for (int i = 0; i < LEVELS; i++) {
      temp.addLevel(new ParkingLevel(LOTS));
    }

    g = temp;
  }

  @After
  public void tearDown() throws Exception {
    g = null;
  }

  @Test
  public void testConstructor() throws Exception {
    assertEquals(LEVELS * LOTS, g.getFreeLotsCount());
    assertNull(g.findVehicle(null));
    assertNull(g.findVehicle(new Car("")));
    assertNull(g.findVehicle(new Motorbike("asdf")));
  }

  @Test
  public void testGetFreeSlotsCount_TryPlaceVehicle_FindVehicle() throws Exception {
    assertEquals(20, g.getFreeLotsCount());
    for (int i = 0; i < 20; i++) {
      IVehicle v = i % 2 == 0 ? new Car("car" + Integer.toString(i))
          : new Motorbike("mb" + Integer.toString(i));
      assertNotNull(g.tryPlaceVehicle(v));
      assertEquals(20 - i - 1, g.getFreeLotsCount());
    }

    assertEquals(0, g.getFreeLotsCount());

    //now find all of them in reverse order
    for (int i = 19; i >= 0; i--) {
      IVehicle v = i % 2 == 0 ? new Car("car" + Integer.toString(i))
          : new Motorbike("mb" + Integer.toString(i));
      ParkingPlace pp = g.findVehicle(v);
      assertNotNull(String.format("Place for %1$s not found", v.getIdentifier()), pp);
      assertEquals(i / LOTS, pp.getLevel());
      assertEquals(i % LOTS, pp.getLot());
      assertEquals(0, g.getFreeLotsCount());
    }
  }

  @Test(timeout = 1000L)
  public void testTryPlaceVehicle_FindVehicle_ReleasePlace() throws Exception {
    // test empty places
    for (int i = 0; i < LEVELS; i++) {
      for (int j = 0; j < LOTS; j++) {
        assertNull(g.releasePlace(new ParkingPlace(i, j)));
        assertNull(g.releasePlace(i, j));
      }
    }

    ArrayList<IVehicle> pushed = new ArrayList<IVehicle>(g.getFreeLotsCount());

    int count = 0;
    int freeSlots = LEVELS * LOTS;
    while (g.getFreeLotsCount() > 0) {
      boolean even = count % 2 == 0;
      IVehicle v = even ? new Car("car" + Integer.toString(count))
          : new Motorbike("mb" + Integer.toString(count));

      ParkingPlace locatedPlace = g.tryPlaceVehicle(v);
      ParkingPlace foundPlace = g.findVehicle(v);
      assertEquals(String.format("Located and found places for car %1$s are different", v),
          locatedPlace, foundPlace);

      pushed.add(v);
      freeSlots--;
      assertEquals(freeSlots, g.getFreeLotsCount());

      // every third step remove one of previous cars
      if (count >= 3 && count % 3 == 0) {
        IVehicle toRemove = pushed.get(pushed.size() - (even ? 2 : 3));
        ParkingPlace pp = g.findVehicle(toRemove);
        assertNotNull(pp);

        IVehicle removed;
        if (even) {
          removed = g.releasePlace(pp.getLevel(), pp.getLot());
        } else {
          removed = g.releasePlace(pp);
        }

        assertEquals(removed, toRemove);
        assertNull(g.findVehicle(toRemove));
        freeSlots++;

        assertEquals(freeSlots, g.getFreeLotsCount());
      }

      count++;
    }
  }

  @Test
  public void testOverloading() throws Exception {
    for (int i = 0; i < LOTS * LEVELS; i++) {
      IVehicle v = new Car("car" + Integer.toString(i));
      ParkingPlace locatedPlace = g.tryPlaceVehicle(v);
    }

    assertEquals(0, g.getFreeLotsCount());
    assertNull(g.tryPlaceVehicle(new Motorbike("sfsfd")));

    assertNotNull(g.releasePlace(1, 3));
    assertEquals(1, g.getFreeLotsCount());

    assertEquals(new ParkingPlace(1, 3), g.tryPlaceVehicle(new Motorbike("sfsfd")));

    assertEquals(0, g.getFreeLotsCount());
    assertNull(g.tryPlaceVehicle(new Car("sfsfd")));

    assertNotNull(g.releasePlace(2, 4));
    assertEquals(1, g.getFreeLotsCount());

    assertEquals(new ParkingPlace(2, 4), g.tryPlaceVehicle(new Car("tttt")));
  }
}
