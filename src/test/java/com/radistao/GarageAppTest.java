package com.radistao;

import org.junit.Test;

/**
 * Simple test just ensure application is executed successfully.
 */
public class GarageAppTest {
  @Test
  public void testSimple() throws Exception {
    GarageApp.main(new String[0]);
  }
}
