package com.radistao.vehicle;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CarTest {
  @Test
  public void testIdentifier() throws Exception {
    assertEquals("345", new Car("345").identifier);
    assertEquals("", new Car("").identifier);
    assertEquals(" ", new Car(" ").identifier);
    assertEquals("adsfadsfasd", new Car("adsfadsfasd").identifier);
  }

  @Test
  public void testEquals() throws Exception {
    assertEquals(new Car("345"), new Car("345"));
    assertEquals(new Car(""), new Car(""));
    assertEquals(new Car(" "), new Car(" "));
    assertEquals(new Car("adsfadsfasd"), new Car("adsfadsfasd"));

    assertNotEquals(new Car(" "), new Car(""));
    assertNotEquals(new Car(""), new Car("0"));
    assertNotEquals(new Car("45"), new Car("54"));
  }

  @Test
  public void testHashCode() throws Exception {
    assertEquals(new Car("345").hashCode(), new Car("345").hashCode());
    assertEquals(new Car("").hashCode(), new Car("").hashCode());
    assertEquals(new Car(" ").hashCode(), new Car(" ").hashCode());
    assertEquals(new Car("adsfadsfasd").hashCode(), new Car("adsfadsfasd").hashCode());

    assertNotEquals(new Car(" ").hashCode(), new Car("").hashCode());
    assertNotEquals(new Car("").hashCode(), new Car("0").hashCode());
    assertNotEquals(new Car("45").hashCode(), new Car("54").hashCode());
  }
}
