package com.radistao.vehicle;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class MotorbikeTest {
  @Test
  public void testIdentifier() throws Exception {
    assertEquals("345", new Motorbike("345").identifier);
    assertEquals("", new Motorbike("").identifier);
    assertEquals(" ", new Motorbike(" ").identifier);
    assertEquals("adsfadsfasd", new Motorbike("adsfadsfasd").identifier);
  }

  @Test
  public void testEquals() throws Exception {
    assertEquals(new Motorbike("345"), new Motorbike("345"));
    assertEquals(new Motorbike(""), new Motorbike(""));
    assertEquals(new Motorbike(" "), new Motorbike(" "));
    assertEquals(new Motorbike("adsfadsfasd"), new Motorbike("adsfadsfasd"));

    assertNotEquals(new Motorbike(" "), new Motorbike(""));
    assertNotEquals(new Motorbike(""), new Motorbike("0"));
    assertNotEquals(new Motorbike("45"), new Motorbike("54"));
  }

  @Test
  public void testHashCode() throws Exception {
    assertEquals(new Motorbike("345").hashCode(), new Motorbike("345").hashCode());
    assertEquals(new Motorbike("").hashCode(), new Motorbike("").hashCode());
    assertEquals(new Motorbike(" ").hashCode(), new Motorbike(" ").hashCode());
    assertEquals(new Motorbike("adsfadsfasd").hashCode(), new Motorbike("adsfadsfasd").hashCode());

    assertNotEquals(new Motorbike(" ").hashCode(), new Motorbike("").hashCode());
    assertNotEquals(new Motorbike("").hashCode(), new Motorbike("0").hashCode());
    assertNotEquals(new Motorbike("45").hashCode(), new Motorbike("54").hashCode());
  }
}
