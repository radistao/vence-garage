package com.radistao.garage;

import com.radistao.exception.GarageException;
import com.radistao.vehicle.IVehicle;

import java.util.ArrayList;

/**
 * Object to process info of every parking floor.
 */
public class ParkingLevel {

  /**
   * List of parking lots. This list has immutable size and capacity equal to {@link #getLotsCount()}.
   * If entry value is <b>null</b> - lot with this index is free, otherwise it holds reference
   * to located vehicle.
   */
  private final ArrayList<IVehicle> lots;

  private int freeLotsCount;

  /**
   * Last found empty slot index.
   * <p>Used for free lot search optimization: very high probable next parking slot to last found
   * is also free.</p>
   *
   * @see Garage#lastFreeLevel
   */
  private int lastFreeLotIndex = 0;

  /**
   * Creates list of parking lots and fills it with <b>null</b>.
   *
   * @param lotsCount Number of lots on the level.
   */
  public ParkingLevel(int lotsCount) {
    this.lots = new ArrayList<IVehicle>(lotsCount);

    for (int i = 0; i < lotsCount; i++) {
      this.lots.add(null);
    }

    this.freeLotsCount = lotsCount;
  }

  /**
   * Number of lots on the level. Value is set in constructor and immutable.
   *
   * @return Number of all lots on the level.
   */
  public int getLotsCount() {
    return lots.size();
  }

  /**
   * Number of free lots on the level. This value is altered every time vehicle is located of left.
   *
   * @return Number of free lots on the level
   */
  public int getFreeLotsCount() {
    return freeLotsCount;
  }

  /**
   * Place vehicle to  {@code lot} index.
   *
   * @param lot     Index where to place vehicle
   * @param vehicle Vehicle to place.
   * @throws GarageException if place already occupied.
   */
  public void placeVehicle(int lot, IVehicle vehicle) {
    if (lots.get(lot) != null) {
      throw new GarageException(String.format("Lot %1$d already occupied", lot));
    }

    lots.set(lot, vehicle);
    freeLotsCount--;

    assert freeLotsCount >= 0 : "Error lots distributing: too many lots are placed";
  }

  /**
   * @param index Lot index
   * @return Vehicle from the place of <b>null</b> if no vehicle there.
   */
  public IVehicle getVehicle(int index) {
    return lots.get(index);
  }

  /**
   * Remove vehicle from {@code lot} place.
   *
   * @param lot place index on the level
   * @return Reference to vehicle if there was any, or <b>null</b> otherwise.
   */
  public IVehicle releasePlace(int lot) {
    IVehicle res = lots.get(lot);
    lots.set(lot, null);
    if (res != null) {
      freeLotsCount++;
      lastFreeLotIndex = lot; // small optimization: next search will find empty place instantly
    }

    assert freeLotsCount <= lots.size() : "Error lots distributing: too many free lots left";

    return res;
  }

  /**
   * Search for free lot in the level. Implementation takes linear time, but with some optimization
   * (see {@link #lastFreeLotIndex})
   *
   * @return zero based index of free lot, or -1 if no free lot found.
   */
  public int findFreeLot() {
    if (freeLotsCount > 0) {
      assert lastFreeLotIndex < lots.size() : "lastFreeLotIndex is bigger than lots size";

      for (int i = 0; i < lots.size(); i++) {
        if (lots.get(lastFreeLotIndex) == null) {
          return lastFreeLotIndex;
        }
        lastFreeLotIndex = (lastFreeLotIndex + 1) % lots.size();
      }

      assert false : "This line should be unreachable: if freeLotsCount > 0, free lot must exist";
    }

    return -1;
  }

  /**
   * Search for vehicle on the level. Implementation takes linear time.
   *
   * @param vehicle Vehicle to find
   * @return zero based lot index, or -1 if not found.
   */
  public int findVehicle(IVehicle vehicle) {
    return placedLotsCount() > 0 ? lots.indexOf(vehicle) : -1;
  }

  /**
   * @return Number of occupied slots in the level
   */
  private int placedLotsCount() {
    return getLotsCount() - freeLotsCount;
  }
}
