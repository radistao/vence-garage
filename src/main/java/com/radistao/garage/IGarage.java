package com.radistao.garage;

import com.radistao.vehicle.IVehicle;

/**
 * This is main application interface, which goes from task description:
 * <pre>
 * Vehicles should be able to enter and exit the garage – the garage should then assign
 * a free space or reject the vehicle if there are no more free parking lots.
 * The manager of the garage should be able to ask the system for the location of a specific
 * vehicle. The response should include the parking level and the assigned parking lot.
 * Also, the number of free parking lots should be queryable.
 * </pre>
 * So, the interface has 4 methods:<ul>
 * <li>{@link #getFreeLotsCount()} - number of free parking lots</li>
 * <li>{@link #tryPlaceVehicle(IVehicle)} - the garage should then assign
 * a free space or reject the vehicle</li>
 * <li>{@link #findVehicle(IVehicle)} - ask the system for the location of a specific
 * vehicle</li>
 * <li>{@link #releasePlace(int, int)} and {@link #releasePlace(ParkingPlace)} - exit the garage</li>
 * </ul>
 */
public interface IGarage {

  /**
   * Number of all free parking lots in the entire garage. This function has constant order of growth.
   *
   * @return Number of free parking lots in the entire garage.
   */
  int getFreeLotsCount();

  /**
   * Function to push a vehicle into first found free parking lot.
   * This function may have linear order of growth and depends on number of levels and lots per level..
   *
   * @param vehicle Vehicle to put to the garage.
   * @return If place is found - return level and lot index where car is placed. Return <b>null</b>
   * if no free lot found.
   */
  ParkingPlace tryPlaceVehicle(IVehicle vehicle);

  /**
   * Find specific vehicle in garage. Function may take linear time order of growth.
   *
   * @param vehicle vehicle to search. Search is processed based on {@link IVehicle#getIdentifier()} value.
   * @return place where vehicle is located, or <b>null</b> if not this vehicle found.
   */
  ParkingPlace findVehicle(IVehicle vehicle);

  /**
   * Remove vehicle from certain place.
   *
   * @param place Place to release.
   * @return Vehicle which was on that place, or <b>null</b> if the lot was empty.
   */
  IVehicle releasePlace(ParkingPlace place);

  /**
   * @param level floor where lot is located
   * @param lot   lot index
   * @return {@link #releasePlace(ParkingPlace)}
   */
  IVehicle releasePlace(int level, int lot);

}
