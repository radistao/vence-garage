package com.radistao.garage;

import com.radistao.vehicle.IVehicle;

import java.util.ArrayList;
import java.util.List;

public class Garage implements IGarage {

  /**
   * List of <code>ParkingLevel</code> floors with lots.
   */
  private final ArrayList<ParkingLevel> levels;

  /**
   * This value is changed every time vehicle is parked of left the garage.
   */
  private int freeLotsCount = 0;

  /**
   * This value is a cumulative summary of all <code>ParkingLevel#getLotsCount()</code>.
   * It is changed every time new level is added to garage.
   */
  private int totalLotsCount = 0;

  /**
   * Level where last empty place was found.<br/>
   * This variable is for local free lots search optimization: when we found next free parking lot
   * keep this index for next search and start searching from that level: very high probable next
   * lot of the level is empty too.
   *
   * @see ParkingLevel#lastFreeLotIndex
   */
  private int lastFreeLevel = 0;

  /**
   * Create garage from list of levels.
   *
   * @param levels list of levels to use in the garage.
   */
  public Garage(List<ParkingLevel> levels) {
    this.levels = new ArrayList<ParkingLevel>(levels.size());

    for (ParkingLevel parkingLevel : levels) {
      this.levels.add(parkingLevel);
      this.freeLotsCount += parkingLevel.getFreeLotsCount();
      this.totalLotsCount += parkingLevel.getLotsCount();
    }
  }

  /**
   * Build empty garage. Using this constructor supposes to use {@link #addLevel(ParkingLevel) addLevel()}
   * method after that.
   */
  public Garage() {
    this.levels = new ArrayList<ParkingLevel>();
  }

  /**
   * Push new level to this garage.
   * <p>For now we allow to add new levels. This wasn't clear described in the task description.
   * Removing levels is not allowed!</p>
   *
   * @param newLevel new parking level object, which is added on top of the levels "stack"
   */
  public void addLevel(ParkingLevel newLevel) {
    levels.add(newLevel);
    freeLotsCount += newLevel.getFreeLotsCount();
    totalLotsCount += newLevel.getFreeLotsCount();
  }

  public int getFreeLotsCount() {
    return freeLotsCount;
  }

  /**
   * Find free place in the garage. This function has linear execution time and depended on number
   * of levels and lots on levels.
   * <p>Some optimization applied to this implementation: we remember last where last free lot
   * was found. As we use consecutive cars placing this allows as to expect with high probability
   * next lot on the same level is free, same as expectation the next level is also free.
   * </p>
   *
   * @return Object with level-lot pair, if empty place exist, otherwise <b>null</b> is returned.
   */
  private ParkingPlace getNextFreePlace() {
    if (getFreeLotsCount() > 0) {
      final int LEVELS = levels.size();
      assert lastFreeLevel < LEVELS : "Wrong lastFreeLevel: greater than levels count";

      for (int i = 0; i < LEVELS; i++) {
        ParkingLevel level = levels.get(lastFreeLevel);

        if (level.getFreeLotsCount() > 0) {
          int j = level.findFreeLot();
          if (j > -1) {
            return new ParkingPlace(lastFreeLevel, j);
          }
        }

        lastFreeLevel = (lastFreeLevel + 1) % LEVELS;
      }

      // this line should be unreachable!
      assert false : "freeLotsCount is zero, but no free lot found on any of levels!";
    }

    return null; // free slots not found
  }

  public ParkingPlace tryPlaceVehicle(IVehicle vehicle) {
    ParkingPlace place = getNextFreePlace();
    if (place != null) {
      levels.get(place.getLevel()).placeVehicle(place.getLot(), vehicle);
      freeLotsCount--;

      assert freeLotsCount >= 0 : "freeLotsCount error: less than 0";
    }

    return place;
  }

  public ParkingPlace findVehicle(IVehicle vehicle) {
    if (placedLotsCount() > 0) {
      for (int i = 0; i < levels.size(); i++) {
        int j = levels.get(i).findVehicle(vehicle);
        if (j > -1) {
          return new ParkingPlace(i, j);
        }
      }
    }

    return null;
  }

  public IVehicle releasePlace(ParkingPlace place) {
    return releasePlace(place.getLevel(), place.getLot());
  }

  public IVehicle releasePlace(int level, int lot) {
    IVehicle vehicle = levels.get(level).releasePlace(lot);

    if (vehicle != null) {
      freeLotsCount++;
      lastFreeLevel = level; // small optimization: next search will find empty place on this level
    }

    return vehicle;
  }

  /**
   * @return Number of occupied lots in entire garage. This function is not a part of requested
   * interface, thus it is used only int internal scope and tests. If interface is expanded
   * in future, function may become public.
   */
  int placedLotsCount() {
    return totalLotsCount - freeLotsCount;
  }

}
