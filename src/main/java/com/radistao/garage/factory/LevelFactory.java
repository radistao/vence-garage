package com.radistao.garage.factory;

import com.radistao.garage.ParkingLevel;

/**
 * Default garage parking level factory. It builds every level with <code>lots</code> number of parking places.
 */
public class LevelFactory implements ILevelFactory {

  private int lots;

  public LevelFactory(int lots) {
    this.lots = lots;
  }

  public ParkingLevel makeLevel() {
    return new ParkingLevel(lots);
  }
}
