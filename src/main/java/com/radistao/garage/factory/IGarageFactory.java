package com.radistao.garage.factory;

import com.radistao.garage.IGarage;

/**
 * Factory to produce craft garage using some kind of <code>levelFactory</code>.
 */
public interface IGarageFactory {

  /**
   * Factory for crafting every level.
   *
   * @return Factory for crafting every level.
   */
  ILevelFactory getLevelFactory();

  void setLevelFactory(ILevelFactory levelFactory);

  /**
   * Build garage using <code>levelFactory</code>.
   *
   * @param levels Number of result garage levels.
   * @return new build garage with <code>levels</code> floors.
   */
  IGarage makeGarage(int levels);
}
