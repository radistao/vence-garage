package com.radistao.garage.factory;

import com.radistao.exception.GarageException;
import com.radistao.garage.Garage;
import com.radistao.garage.IGarage;

public class GarageFactory implements IGarageFactory {
  private ILevelFactory levelFactory;

  public GarageFactory(ILevelFactory levelFactory) {
    this.levelFactory = levelFactory;
  }

  public ILevelFactory getLevelFactory() {
    return levelFactory;
  }

  public void setLevelFactory(ILevelFactory levelFactory) {
    this.levelFactory = levelFactory;
  }

  public IGarage makeGarage(int levels) {
    if (levelFactory == null) {
      throw new GarageException("GarageFactory's levelFactory is null");
    }

    Garage g = new Garage();

    for (int i = 0; i < levels; i++) {
      g.addLevel(levelFactory.makeLevel());
    }

    return g;
  }
}
