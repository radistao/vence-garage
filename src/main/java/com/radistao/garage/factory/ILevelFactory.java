package com.radistao.garage.factory;

import com.radistao.garage.ParkingLevel;

/**
 * Factory to craft one certain level of garage.
 */
public interface ILevelFactory {
  ParkingLevel makeLevel();
}
