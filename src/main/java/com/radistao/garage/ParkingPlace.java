package com.radistao.garage;

/**
 * Level-lot indexes integer pair to place, remove and search vehicles  in garage.
 */
public class ParkingPlace {
  private int level;
  private int lot;

  public ParkingPlace(int level, int lot) {
    if (level < 0 || lot < 0) {
      throw new IllegalArgumentException("level and lot must be non-negative");
    }
    this.level = level;
    this.lot = lot;
  }

  public int getLevel() {
    return level;
  }

  public int getLot() {
    return lot;
  }

  @Override
  public int hashCode() {
    int hash = 17;
    hash = hash * 31 + level;
    hash = hash * 31 + lot;
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    ParkingPlace that = (ParkingPlace) obj;

    return this.level == that.level && this.lot == that.lot;
  }

  @Override
  public String toString() {
    return String.format("[level=%1$d, lot=%2$d]", level, lot);
  }
}
