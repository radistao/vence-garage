package com.radistao.vehicle;

/**
 * Basic interface to work with vehicles in the garage. For current task description only
 * {@link #getIdentifier()} is required for vehicles in a garage.
 */
public interface IVehicle {

  /**
   * There was no in task description requirement about vehicle identifier mutability,
   * it should be discussed with customer before real implementation.
   * For now we suppose it is immutable (at least inside garage).
   *
   * @return unique vehicle identifier (plate).
   */
  String getIdentifier();
}
