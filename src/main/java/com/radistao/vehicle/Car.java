package com.radistao.vehicle;

/**
 * See {@link AbstractVehicle}
 */
public class Car extends AbstractVehicle {

  public Car(String identifier) {
    super(identifier);
  }
}
