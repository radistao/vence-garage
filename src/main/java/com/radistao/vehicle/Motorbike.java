package com.radistao.vehicle;

/**
 * See {@link AbstractVehicle}
 */
public class Motorbike extends AbstractVehicle {
  public Motorbike(String identifier) {
    super(identifier);
  }
}
