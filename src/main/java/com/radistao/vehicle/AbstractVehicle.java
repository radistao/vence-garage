package com.radistao.vehicle;

/**
 * Basic abstract class for all vehicles. It implements {@link #getIdentifier()} and basic entity
 * methods: {@link #equals(Object)}, {@link #hashCode()}, {@link #toString()}, which all are based
 * on {@link #identifier} value.
 * <p>For now all children classes ({@link Car} and {@link Motorbike} use this implementation
 * without overriding, as it wasn't described in the task.</p>
 */
public class AbstractVehicle implements IVehicle {

  protected final String identifier;

  public AbstractVehicle(String identifier) {
    this.identifier = identifier;
  }

  public String getIdentifier() {
    return identifier;
  }

  @Override
  public int hashCode() {
    // while identifier is
    return identifier.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (obj == this) {
      return true;
    }

    if (obj == null || obj.getClass() != this.getClass()) {
      return false;
    }

    return identifier.equals(((AbstractVehicle) obj).identifier);
  }

  @Override
  public String toString() {
    return getIdentifier();
  }
}
