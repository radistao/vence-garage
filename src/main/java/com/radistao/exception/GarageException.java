package com.radistao.exception;

/**
 * These exceptions should be extended later if we need more specific exceptions raiser than
 * NullPointer, IllegalArgument or IndexOutOfBounds.
 */
public class GarageException extends RuntimeException {
  public GarageException(String message) {
    super(message);
  }
}
