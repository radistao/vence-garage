package com.radistao;

import com.radistao.garage.IGarage;
import com.radistao.garage.ParkingPlace;
import com.radistao.garage.factory.GarageFactory;
import com.radistao.garage.factory.LevelFactory;
import com.radistao.vehicle.Car;
import com.radistao.vehicle.IVehicle;
import com.radistao.vehicle.Motorbike;

/**
 * Garage application demonstration: using factory build garage and play with it: put vehicles
 * to garage, remove, search, query empty places.
 */
public class GarageApp {
  public static void main(String[] args) {
    System.out.println("\nGarage application simulator:");

    GarageFactory gf = new GarageFactory(new LevelFactory(10));
    System.out.println("Create garage with 6 levels, 10 lots per level");

    IGarage garage = gf.makeGarage(6);

    System.out.printf("New garage has %1$d free parking lots\n", garage.getFreeLotsCount());

    Car car1 = new Car("KZ652");
    ParkingPlace car1Place = garage.tryPlaceVehicle(car1);

    System.out.printf("Here is new car '%1$s' coming to place %2$s\n", car1, car1Place);
    System.out.printf("Now %1$d free parking lots left in the garage\n", garage.getFreeLotsCount());

    Motorbike mb1 = new Motorbike("HB135");
    ParkingPlace mb1Place = garage.tryPlaceVehicle(mb1);

    System.out.printf("Here is new motorbike '%1$s' coming to place %2$s\n", mb1, mb1Place);
    System.out.printf("Now %1$d free parking lots left in the garage\n", garage.getFreeLotsCount());

    System.out.println("Try to find some vehicles:");
    System.out.printf("Car '%1$s' is on place %2$s\n", car1, garage.findVehicle(car1));
    System.out.printf("Motorbike '%1$s' is on place %2$s\n", mb1, garage.findVehicle(mb1));

    Car car2 = new Car("789 HY");
    System.out.printf("Check whether we have '%1$s' garage:\n", car2);
    System.out.printf("Car '%1$s' is placed on %2$s \n", car2, garage.findVehicle(car2));

    System.out.println("Put this car to the garage:");
    garage.tryPlaceVehicle(car2);
    System.out.printf("Now car '%1$s' is placed on %2$s \n", car2, garage.findVehicle(car2));
    System.out.printf("And %1$d free parking lots left in the garage\n", garage.getFreeLotsCount());

    System.out.printf("Car '%1$s' is leaving garage:\n", car1);
    garage.releasePlace(car1Place);
    System.out.printf("Thus %1$d free parking lots now in the garage\n", garage.getFreeLotsCount());
    System.out.printf("And car '%1$s' is placed on %2$s place\n", car1, garage.findVehicle(car1));

    System.out.printf("Now add %1$d random cars - fill whole garage\n", garage.getFreeLotsCount());

    while (garage.getFreeLotsCount() > 0) {
      Car c = new Car(getRandomIdentifier());
      ParkingPlace p = garage.tryPlaceVehicle(c);
      System.out.printf("Added car '%1$s' to place %2$s, %3$d lots left\n", c, p, garage.getFreeLotsCount());
    }

    System.out.printf("And car '%1$s' is returning, what place could we found for it?\n", car1);

    ParkingPlace noPlace = garage.tryPlaceVehicle(car1);

    System.out.println(noPlace);

    System.out.println("Sorry, no place for you");
    System.out.println("Wait a moment, someone is leaving...");

    IVehicle leftCar = garage.releasePlace(5, 5);

    System.out.printf("Ok, '%1$s' left garage!\n", leftCar);
    System.out.printf("And %1$d free parking lots now in the garage\n", garage.getFreeLotsCount());

    System.out.printf("'%1$s', are you filling good on your new place %2$s?\n", car1, garage.tryPlaceVehicle(car1));

    System.out.println("");
  }

  private static String getRandomIdentifier() {
    return new StringBuilder()
        .append(getRandomChar())
        .append(getRandomChar())
        .append(" ")
        .append(Integer.toString((int) (Math.random() * 10000)))
        .append(getRandomChar())
        .append(getRandomChar()).toString();
  }

  private static char getRandomChar() {
    return (char) ('A' + (int) (Math.random() * ALPHA_NUM));
  }

  private static final int ALPHA_NUM = 26;
}
